https://flowbite-svelte.com/pages/getting-started

Installing from scratch
SvelteKit
You can install SvelteKit or Svelte to start your app. For SvelteKit:
```
npm create svelte@latest my-app
cd my-app
npm install
```


If you want to get started with Svelte:
```
npm create vite@latest myapp -- --template svelte
cd myapp
npm install
```

Install Tailwind CSS
```
npx svelte-add@latest tailwindcss
npm i
```

Run it:
```
npm run dev
```

Install dependencies
```
npm i -D flowbite flowbite-svelte classnames @popperjs/core
```

Update tailwind.config.cjs
```
    const config = {
    content: [
        "./src/**/*.{html,js,svelte,ts}",
        "./node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}",
    ],

    theme: {
        extend: {},
    },

    plugins: [
        require('flowbite/plugin')
    ],
    darkMode: 'class',
    };

    module.exports = config;
```
Now you are ready to go! 
Add the following to `src/routes/+page.svelte`.
```
<script>
	import { Alert } from 'flowbite-svelte';
</script>

<div class="p-8">
	<Alert>
		<span class="font-medium">Info alert!</span> Change a few things up and try submitting again.
	</Alert>
</div>
```

If you see the following image, then your setting is complete.

> Info alert! Change a few things up and try submitting again.
