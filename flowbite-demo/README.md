# Flowbite-Svelte-Starter

## Demo

[Demo site](https://flowbite-svelte-starter.vercel.app/)

## Feature

- Svelte
- SvelteKit
- Tailwind CSS
- Flowbite
- Flowbite-Svelte
- Flowbite-Svelte-Blocks
- ESlint
- Typescript
- Playwright
- Prettier
- Svelte-heros(Heroicons)
- Darkmode activated

## Installation

```sh
npx degit shinokada/flowbite-svelte-starter my-demo
cd my-demo
pnpm i // or npm i
pnpm run dev // or npm run dev
```

Then update dependencies:

```
pnpm update
// you may need to run this:
pnpm i -D flowbite-svelte@latest
```

## Other examples/themes

### Svelte-Sidebar-Example

If you want to see [a svelte-sidebar demo](https://flowbite-svelte-starter.vercel.app/):

```sh
git checkout svelte-sidebar-example
```

### Jaco example

```sh
git checkout jaco
```

### Plain

```sh
git checkout plain
```
