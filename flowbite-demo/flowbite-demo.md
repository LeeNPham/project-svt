https://flowbite-svelte.com/extend/flowbite-svelte-starter

Features
Svelte
SvelteKit
Tailwind CSS
Flowbite
Flowbite-Svelte
Flowbite-Svelte-Blocks
ESlint
Typescript
Playwright
Prettier
Svelte-heros(Heroicons)
Darkmode activated

Installation:
```
npx degit shinokada/flowbite-svelte-starter my-demo
cd my-demo
npm i
npm run dev
```

Other examples/themes:
* Svelte-Sidebar-Example
If you want to see a svelte-sidebar demo:
```
git checkout svelte-sidebar-example
```

* Jaco example
```
git checkout jaco
```

* Plain
```
git checkout plain
```