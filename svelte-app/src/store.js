import {writable} from 'svelte/store';

export const randomStore = writable(Math.random());

// we can set the value by using set or update

randomStore.update(v => v + 1)

// if we want to listen to the value (v), we will use the subscribe method
// make sure to manage subscriptions properly or there will be memory leaks
// randomStore.subscribe(v => v)

