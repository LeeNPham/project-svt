> References: https://fireship.io/lessons/svelte-v3-overview-firebase/


Command lines used:
```
npx degit sveltejs/template svelte-app

```

cd into svelt-app directory

> install dependencies and update package.json files within src
```
npm i
```

> serve the application locally
```
npm run dev
```

> using observable stores (similar to redux/flux)
```
app.svelte > main.js > index.html

<script>
    import { writable } from 'svelte/store';
    const user = writable({icon:👻});
</script> 
// $ stands for subscribe
{$user.icon}

```

changing strings to indicate markdown related html tags
```
<script>
	let string = `this string contains some <strong>HTML!!!</strong>`;
</script>

<p>{@html string}</p>
```



> rollup.config.js
```
This is what bundles our code and acts as an alternative to webpack
in bundling code, there are 2 things we should focus on
svelte supports SSR(server side rendering) and Custom Elements
```

the biggest difference between svelte and other js frameworks
is that if you goto your package.json
you'll notice that you're missing dependencies, 
the only ones there are developer dependencies such as rollup and svelte.
(this is because it's a compiler!)

   > it generates all of your code at build time 
   > which means it doesnt need to include the framework itself as a hard dependency in your js bundle.
   > (another framework that does this is stencil.js but this approach makes a lot more sense than the current status quo )
    >> you only need to compile the code from the js, meaning no extra bloat in our js bundle 

it exports our entire app as a string, and if we goto the bundle.js, 


event handling:
    on:click
    on:mouseevent

look up <slot></slot>
similar to extends html in django
use it to instantiate a html tag using the class structure that the nested svelte file should return. 
you'll use slot slot in the component file and you'll add actual data in the app.svelte.

you can set up a function to reference back to a re-computed value with the $: syntax. that way you can call its result as a variable


in terms of reactivity, 
we can bind to the attribute within DOM elements (such as input)
```
<input bind:value={rando}>
```















Sveltekit allows for server side rendering, routing, code splitting